Blockly.Blocks['question_operation_answer'] = {
    init: function() {
      this.appendValueInput("question")
          .setCheck(["Number", "String"])
          .appendField("Kérdés:");
      this.appendStatementInput("operations")
          .setCheck(null);
      this.appendValueInput("eredmeny_legyen")
          .setCheck(["String", "Number"])
          .appendField("Eredmény legyen");
      this.appendDummyInput();
      this.appendValueInput("answer")
          .setCheck(["Number", "String"])
          .appendField("Válasz:");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(30);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };
  Blockly.Blocks['start_talking'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("Beszélgetés kezdődik");
      this.setNextStatement(true, null);
      this.setColour(30);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };

  Blockly.Blocks['input_a'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("A");
      this.setOutput(true, null);
      this.setColour(30);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };

  Blockly.Blocks['input_b'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("B");
      this.setOutput(true, null);
      this.setColour(30);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };


  Blockly.Blocks['input_c'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("C");
      this.setOutput(true, null);
      this.setColour(30);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };
  Blockly.Blocks['output'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("Eredmény");
      this.setOutput(true, null);
      this.setColour(30);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };

Blockly.Blocks['write'] = {
    init: function() {
      this.appendValueInput("input")
          .appendField("Kiír")
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(45);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };