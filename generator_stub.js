Blockly.JavaScript['question_operation_answer'] = function(block) {
  var value_question = Blockly.JavaScript.valueToCode(block, 'question', Blockly.JavaScript.ORDER_ATOMIC);
  var statements_operations = Blockly.JavaScript.statementToCode(block, 'operations');
  var value_eredmeny_legyen = Blockly.JavaScript.valueToCode(block, 'eredmeny_legyen', Blockly.JavaScript.ORDER_ATOMIC);
  var value_answer = Blockly.JavaScript.valueToCode(block, 'answer', Blockly.JavaScript.ORDER_ATOMIC);
  var substring = value_answer.replace('String','');
  var subsubstring = substring.replace('String','');
  var code =
  'window.alert("Sanyi bácsi a következőt fogja megoldani: "+'+ value_question+'); ctx.beginPath(); ctx.fillStyle = "white"; ctx.arc(270,130,110,0,2*Math.PI); ctx.fill();\n' + 
   statements_operations+ '\n' + 
  'eredmeny=' + value_eredmeny_legyen + ';\n' +
  'ctx.font = "bold '+Math.round(350/(subsubstring.length/1.2))+'px Arial";\n'+
  'ctx.fillStyle ="black"; ctx.fillText(' + value_answer + ',220,140);\n';
  return code;
};
Blockly.JavaScript['start_talking'] = function(block) {
  var code = 
  'var canvas = document.getElementById("canvas");\n'+
  'var ctx = canvas.getContext("2d");\n'+
  'ctx.font = "18px Arial";\n'+
  'var a = document.getElementById("textfield_a").value;\n'+
  'var b = document.getElementById("textfield_b").value;\n'+
  'var c = document.getElementById("textfield_c").value;\n'+
  'var eredmeny;\n';
  return code;
};

Blockly.JavaScript['input_a'] = function(block) {
  var code = 'a';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['input_b'] = function(block) {
  var code = 'b';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['input_c'] = function(block) {
  var code = 'c';
  return [code, Blockly.JavaScript.ORDER_NONE];
};
Blockly.JavaScript['output'] = function(block) {
  var code = 'eredmeny';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['write'] = function(block) {
  var value_input = Blockly.JavaScript.valueToCode(block, 'input', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 
  'window.alert(' +value_input + ');\n';
  return code;
};